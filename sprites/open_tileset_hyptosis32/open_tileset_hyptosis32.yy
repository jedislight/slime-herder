{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 2431,
  "bbox_top": 0,
  "bbox_bottom": 1918,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 2432,
  "height": 1919,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"90e76486-b846-47f6-9a23-da91e2156497","path":"sprites/open_tileset_hyptosis32/open_tileset_hyptosis32.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"90e76486-b846-47f6-9a23-da91e2156497","path":"sprites/open_tileset_hyptosis32/open_tileset_hyptosis32.yy",},"LayerId":{"name":"dca2cb95-c4e0-473e-ab50-62beecc3f7a3","path":"sprites/open_tileset_hyptosis32/open_tileset_hyptosis32.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"open_tileset_hyptosis32","path":"sprites/open_tileset_hyptosis32/open_tileset_hyptosis32.yy",},"resourceVersion":"1.0","name":"90e76486-b846-47f6-9a23-da91e2156497","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"open_tileset_hyptosis32","path":"sprites/open_tileset_hyptosis32/open_tileset_hyptosis32.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1445bd2a-156a-45a4-b201-05a8537c7d8d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"90e76486-b846-47f6-9a23-da91e2156497","path":"sprites/open_tileset_hyptosis32/open_tileset_hyptosis32.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"open_tileset_hyptosis32","path":"sprites/open_tileset_hyptosis32/open_tileset_hyptosis32.yy",},
    "resourceVersion": "1.3",
    "name": "open_tileset_hyptosis32",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"dca2cb95-c4e0-473e-ab50-62beecc3f7a3","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Tile Sets",
    "path": "folders/Tile Sets.yy",
  },
  "resourceVersion": "1.0",
  "name": "open_tileset_hyptosis32",
  "tags": [],
  "resourceType": "GMSprite",
}