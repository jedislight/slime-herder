/// @description Insert description here
// You can write your code in this editor
if (instance_number(Gem) != 0 ) exit;
instance_destroy(other.id)
if( instance_number(Slime) == 0 ) {
	next_room = room;
	do {
		next_room = room_next(next_room);
	} until (!room_exists(next_room) or !asset_has_any_tag(next_room, ["TEMPLATE"], asset_room) );
	if ( room_exists(next_room)) {
		room_goto(next_room);	
	} else {
		game_restart();	
	}
}