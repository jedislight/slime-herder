/// @description Insert description here
// You can write your code in this editor
sprite_index = Sprite1;
enum MOVE_MODE {
	SINGLE,	
	SLIDE,
	HUNGRY,
	JUMPER,
	GEM,
}

image_blend = c_lime;
image_alpha = 0.65;

selected = false;
move_mode = MOVE_MODE.SINGLE;