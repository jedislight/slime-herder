/// @description Move Confirmation
show_debug_message("SLIME MOVE")

function create_custom_trail(xx, yy, object_type) {
	var n = instance_create_layer(xx,yy,"FLOOR", object_type);
	n.image_blend = image_blend;
	n.move_mode = move_mode;
	n.type = object_index;
	n.image_alpha = image_alpha;
}

function create_goo(xx, yy) {
	var n = instance_create_layer(xx,yy,"FLOOR", Goo);
	n.image_blend = image_blend;
	n.move_mode = move_mode;
	n.type = object_index;
	n.image_alpha = image_alpha;
}

switch(move_mode) {
	default:
	case MOVE_MODE.HUNGRY:
	case MOVE_MODE.SINGLE:
	case MOVE_MODE.JUMPER:
		create_goo(x,y);
		x = MoveSquare.x
		y = MoveSquare.y;
		
		break;
	case MOVE_MODE.SLIDE:
		sx = sign(MoveSquare.x - x)
		sy = sign(MoveSquare.y - y);
		d = abs(MoveSquare.x - x) div 100 + abs(MoveSquare.y - y) div 100;
		var xx = x;
		var yy = y;
		for (var i = 0; i < d; ++i) {
			create_goo(xx,yy);
			xx += sx * 100;
			yy += sy * 100;
		}
		x = MoveSquare.x
		y = MoveSquare.y;
		
		break;
	case MOVE_MODE.GEM:
		create_custom_trail(x,y, Gem);
		x = MoveSquare.x
		y = MoveSquare.y;
		
		break;
}