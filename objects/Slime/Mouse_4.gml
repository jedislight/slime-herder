/// @description Insert description here
// You can write your code in this editor
with(Slime) {selected = false};
selected = true;
with(Slime)solid=true;
show_debug_message("SLIME SELECTED")

function create_all_but_first_steps_move_square_on_step(xs,ys) {
	var xx = x + xs;
	var yy = y + ys;
	squares = []
	
	while(true){
		xx += xs;
		yy += ys;
		if (!point_in_rectangle(xx, yy, 0,0, room_width, room_height)) {
			break;	
		}
		var newSquare = create_move_square_if_free(xx, yy);
		array_push(squares, newSquare);
	}
		
	return squares;
}

function create_move_square_if_free(xx, yy) {
	if (place_free(xx, yy)) {
		return instance_create_layer(xx, yy, "UI", MoveSquare);
		
	}
	
	return noone;
}

function create_final_step_move_square_on_step(xs, ys) {
	var xx = x + xs;
	var yy = y + ys;
	var square = create_move_square_if_free(xx, yy);
	if (instance_exists(square)) {
		while(true){
			xx += xs;
			yy += ys;
			if (!point_in_rectangle(xx, yy, 0,0, room_width, room_height)) {
				if(instance_exists(square))  {
					instance_destroy(square);	
				}
				return noone;
			}
			if (instance_exists(collision_rectangle(xx, yy, xx + sprite_width, yy+sprite_height, Portal, false, true))) {
				if(instance_exists(square))  {
					instance_destroy(square);	
				}
				return 	create_move_square_if_free(xx, yy);
			}
			var newSquare = create_move_square_if_free(xx, yy);
			if (instance_exists(newSquare)) {
				instance_destroy(square);
				square = newSquare;
			} else {
				break;
			}
		}
		
		return square;
	}
}

switch(move_mode) {
	default:
	case MOVE_MODE.GEM:
	case MOVE_MODE.SINGLE:
		create_move_square_if_free(x+sprite_width, y)
		create_move_square_if_free(x-sprite_width, y)
		create_move_square_if_free(x, y-sprite_height)
		create_move_square_if_free(x, y+sprite_height)
		break;
	case MOVE_MODE.SLIDE:
		create_final_step_move_square_on_step(sprite_width, 0);
		create_final_step_move_square_on_step(-sprite_width, 0);
		create_final_step_move_square_on_step(0, sprite_height);
		create_final_step_move_square_on_step(0, -sprite_height);
		break;
	case MOVE_MODE.HUNGRY:
		instance_deactivate_object(Goo)
		create_move_square_if_free(x+sprite_width, y)
		create_move_square_if_free(x-sprite_width, y)
		create_move_square_if_free(x, y-sprite_height)
		create_move_square_if_free(x, y+sprite_height)
		instance_activate_object(Goo)
		break;
		
	case MOVE_MODE.JUMPER:
		create_all_but_first_steps_move_square_on_step(sprite_width, 0);
		create_all_but_first_steps_move_square_on_step(-sprite_width, 0);
		create_all_but_first_steps_move_square_on_step(0, sprite_height);
		create_all_but_first_steps_move_square_on_step(0, -sprite_height);
		break;
}
	
with(Slime)solid=false;
